<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $table='pictures';
    // public $timestamps=true;
    protected $fillable=['Emp_id','Pic_Path','flag','pic_id'];
    protected $primaryKey = 'pic_id';

}
