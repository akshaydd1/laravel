<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
        protected $table='cities';
    // public $timestamps=true;
    protected $fillable=['state_id','Country_id','City_name'];
    protected $primaryKey = 'City_id';
}
// City_id | Country_id | state_id | City_name