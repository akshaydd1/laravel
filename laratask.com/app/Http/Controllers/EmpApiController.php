<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Country;
use App\City;
use App\State;

use App\Picture;
use DB;

class EmpApiController extends Controller
{


    // public function emppic(){
    //         $Pic=new Picture();


    //     $Pic->pic_id=$request->input('pic_id');
    //     $Pic->Emp_name=$request->input('Pic_Path');
    //     $Pic->Emp_name=$request->input('Pic_Path');
  
  
    // }



    
   public function create(Request $request)
    {
    	$Employe=new Employee();
    	
    	$Employe->Org_id=$request->input('Org_id');
	  	$Employe->Emp_name=$request->input('Emp_name');
  

    	$Employe->Emp_lastname=$request->input('Emp_lastname');
        $Employe->Email=$request->input('Email');
        
    	$Employe->Phone=$request->input('Phone');

    	$Employe->loc_add=$request->input('loc_add');
    	$Employe->Country_id=$request->input('Country_id');

    	$Employe->state_id=$request->input('state_id');
    	$Employe->City_id=$request->input('City_id');
        $Employe->is_enabled=$request->input('is_enabled');
    	// $Employe->save();
    	// return response()->json($Employe);


         if($Employe->Emp_name == NULL || $Employe->Emp_lastname == NULL ||$Employe->Email == NULL||$Employe->Phone == NULL||$Employe->loc_add == NULL||$Employe->is_enabled == NULL)
        {
            $msg =[
                 'message' => 'Organization name is required!!!!',
                  'result' => 'fail'
              ];
              return response()->json($msg);

         }
        else
        {
            $msg =[
             'message' => 'New Data inserted Successfully!!!!',
             'result' => 'pass'
                ];
                
              $Employe->save();
              return response()->json($msg);

        } 




    }


     public function show()
    {
    	$Employe=Employee::all();


         foreach ($Employe as $key => $value) 
        {
     $Employe[$key]['organisation']= DB::table('organizations')->where('Org_id', $value["Org_id"])->value('Org_name');     
       
        $Employe[$key]['country']= DB::table('countries')->where('Country_id', $value["Country_id"])->value('Country_name');
   
       
        $Employe[$key]['state']= DB::table('states')->where('state_id', $value["state_id"])->value('State_name');
         $Employe[$key]['city']= DB::table('cities')->where('City_id', $value["City_id"])->value('City_name');




$Employe[$key]['path'] = DB::table('pictures')->where('Emp_id', $value["Emp_id"])->value('Pic_Path');
$Employe[$key]['flag'] = DB::table('pictures')->where('Emp_id', $value["Emp_id"])->value('flag');


$Employe[$key]['flag'] = DB::table('pictures')->where('Emp_id', $value["Emp_id"])->value('flag');

$Employe[$key]['pic_col'] = DB::table('pictures')->where('Emp_id', $value["Emp_id"])->pluck('Pic_Path');
$Employe[$key]['pic_flag'] = DB::table('pictures')->where('Emp_id', $value["Emp_id"])->pluck('flag');


// foreach ($titles as $title) {
//     echo $title;
// }


        } 


    	return response()->json($Employe);
    }





    public function showbyid(Request $request,$Org_iddq)
    {
        $Employe=Employee::find($Org_iddq);

      


         $Employe->Country = DB::table('countries')->where('Country_id', $Employe["Country_id"])->value('Country_name');
$Employe->Organization = DB::table('organizations')->where('Org_id', $Employe["Org_id"])->value('Org_name');
$Employe->State = DB::table('states')->where('state_id', $Employe["state_id"])->value('State_name');
$Employe->City = DB::table('cities')->where('City_id', $Employe["City_id"])->value('City_name');

$Employe->Pic_Path = DB::table('pictures')->where('Emp_id', $Employe["Emp_id"])->value('Pic_Path');


$Employe->Pic_col= DB::table('pictures')->where('Emp_id', $Employe["Emp_id"])->pluck('Pic_Path');
$Employe->Pic_flag= DB::table('pictures')->where('Emp_id', $Employe["Emp_id"])->pluck('flag');


// print_r(expression)
        // return $Employe; 
        return response()->json($Employe);
    }


    public function updatebyid(Request $request,$Org_id)
    {
        $Employe=Employee::find($Org_id);
     	$Employe->Org_id=$request->input('Org_id');
	  	$Employe->Emp_name=$request->input('Emp_name');
  

    	$Employe->Emp_lastname=$request->input('Emp_lastname');
        $Employe->Email=$request->input('Email');
    	$Employe->Phone=$request->input('Phone');

    	$Employe->loc_add=$request->input('loc_add');
    	$Employe->Country_id=$request->input('Country_id');

    	$Employe->state_id=$request->input('state_id');
    	$Employe->City_id=$request->input('City_id');
         $Employe->is_enabled=$request->input('is_enabled');
        //     $Employe->save();

        // return response()->json($Employe);


           if($Employe->Emp_name == NULL || $Employe->Emp_lastname == NULL ||$Employe->Email == NULL||$Employe->Phone == NULL||$Employe->loc_add == NULL||$Employe->is_enabled == NULL)
        {
            $msg =[
                 'message' => 'Organization name is required!!!!',
                  'result' => 'fail'
              ];
              return response()->json($msg);

         }
        else
        {
            $msg =[
             'message' => 'Data Edited Successfully!!!!',
             'result' => 'pass'
                ];
                
              $Employe->save();
              return response()->json($msg);

        } 











         
    }



    public function deletebyid(Request $request,$Org_idd)
    {
        $Employe=Employee::find($Org_idd);



// $Employe->update(['is_deleted'=>1]);


        // return $Employe; 
        $Employe->delete();
        return response()->json($Employe);
    }


    

}
