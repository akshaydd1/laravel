<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\Country;
use App\City;
use App\State;

use App\Picture;
use DB;
class PictureController extends Controller
{
    
    public function create($id)

    {
        $url="http://laratask.com/api/empshowid/$id";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $someArray = json_decode($output, true);

        $country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
        $state = DB::table('states')->where('state_id', $someArray['state_id'])->value('State_name');

        $city = DB::table('cities')->where('City_id', $someArray['City_id'])->value('City_name');
        $org = DB::table('organizations')->where('Org_id', $someArray['Org_id'])->value('Org_name');


        $someArray['Country_id'] = $country;
        $someArray['state_id'] = $country;

        $someArray['City_id'] = $country;
        $someArray['Org_id'] = $country;

// echo "<pre>";print_r($someArray);
// exit;





    	// $someArray
        // return view('pages.emppic');
        return view('pages.emppic',compact("id","someArray"));
// \
    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

// print_r($request->all()) ;
// exit;

        $this->validate($request, [
        	// 'Emp_id'=>'required',

                'Pic_Path' => 'required',

                'Pic_Path.*' => 'mimes:doc,pdf,docx,zip,png,jpg,jpeg',
                'flag'=>'required'


        ]);



        if($request->hasfile('Pic_Path'))

         {

            foreach($request->file('Pic_Path') as $file)

            {

                $name=$file->getClientOriginalName();

                $file->move(public_path().'/files/', $name);  

                $data[] = $name;  

            }

         }


         foreach($data as $name){
         $file= new Picture();
         $file->Emp_id=$request->input('Emp_id');
         $file->flag=$request->input('flag');
        
         $file->Pic_Path=$name;
// return response()->json($Employe);
         $file->save();
     }



        return back()->with('success', 'Data Your files has been successfully added');

    }

}
