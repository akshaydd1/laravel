<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InsData;
use App\Country;
use Illuminate\Support\Facades\Validator;
use DB;


class ApiController extends Controller
{
    public function create(Request $request)
    {




                $organisation=new Insdata();
                $organisation->Org_name=$request->input('Org_name');

                $organisation->Country_id=$request->input('Country_id');
                // $organisation->save();
                // return response()->json($organisation);

     

                if($organisation->Org_name == NULL)
        {
            $msg =[
                 'message' => 'Organization name is required!!!!',
                  'result' => 'fail'
              ];
              return response()->json($msg);

         }
        else
        {
            $msg =[
             'message' => 'New Data inserted Successfully!!!!',
             'result' => 'pass'
                ];
                
              $organisation->save();
              return response()->json($msg);

        } 




     

    }

    public function show()
    {
    	$organisation=Insdata::all();


        foreach ($organisation as $key => $value) 
        {
        $organisation[$key]['country']= DB::table('countries')->where('Country_id', $value["Country_id"])->value('Country_name');
        }       

        
       // $organisation=InsData::orderBy('Org_id','asc')->paginate(4);

    	return response()->json($organisation);
    }



    public function showbyid(Request $request,$Org_iddq)
    {
        $organisation=Insdata::find($Org_iddq);


        // $organisations = Organisation::find($org_id);

        $organisation->Organization_name = DB::table('countries')->where('Country_id', $organisation["Country_id"])->value('Country_name');


        // return $organisation; 
        return response()->json($organisation);
    }



  // public function flagid(Request $request,$id)
  //   {
  //      $flag=Picture::find($id);
  //       $flag->tag_1=$request->input('flag');

  //             $flag->save();

  //               return response()->json($flag);
  //     // return $id;


  // } 


    public function updatebyid(Request $request,$Org_id)
    {
        $organisation=Insdata::find($Org_id);
        $organisation->Org_name=$request->input('Org_name');
        $organisation->Country_id=$request->input('Country_id');
          

        if($organisation->Org_name == NULL)
        {
            $msg =[
                 'message' => 'Organization name is required!!!!',
                  'result' => 'fail'
              ];
              return response()->json($msg);

         }
        else
        {
            $msg =[
             'message' => 'Data Edited Successfully!!!!',
             'result' => 'pass'
                ];

              $organisation->save();
              return response()->json($msg);

        } 



        //     $organisation->save();

        // return response()->json($organisation);
    }



    public function deletebyid(Request $request,$Org_idd)
    {
        $organisation=Insdata::find($Org_idd);
        // return $organisation; 
        $organisation->delete();
        return response()->json($organisation);
    }

  //  $msg =[
  //    'message' => ' Data deleted Successfully!!!!',
  //    'result' => 'pass'
  // ];


}


