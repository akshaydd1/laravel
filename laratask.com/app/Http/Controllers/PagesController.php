<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Library\Curls;
use Illuminate\Support\Facades\DB;
use App\Country;
use App\City;
use App\State;
use App\InsData;
use App\Employee;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Picture;
class PagesController extends Controller
{




// public function changeflag()
// {
// 	// return 'INDEX';

// 	return view('pages.index')->with('title',$title);
// 	// return view('pages.index', compact('title'));
// }  



public function index()
{
	// return 'INDEX';
	$title="Employee Management";
	return view('pages.index')->with('title',$title);
	// return view('pages.index', compact('title'));
}  

// public function emppic()
// {
// 			 // $countryList = Country::all()->toArray();
// 		 	//  $stateList = State::all()->toArray();
// 		 	//   $cityList = City::all()->toArray();
// // 		 	   $orgList = InsData::all()->toArray();

// // // return view('pages.empinsert',compact('stateList','countryList','cityList','orgList'));
// //  		return view('pages.empinsert',compact('stateList','countryList','cityList','orgList'));

// 	return view('pages.emppic');

// }  
// public function emppic($id)
// {		

// 			$message = [
//         'required' => 'Please fill the above field',
//         'success' => 'Thank You',
// ];

// $this->validate($request, [
//          	'Pic_Path'=>'required',
//          	'flag'=>'required'
//          	 ], $message);



// 		$Org_id=$request->input('Org_id');
// 		$flag=$request->input('flag');
		
// 		$data=['Pic_Path'=>$Pic_Path,'flag'=>$flag];


// 			$data=json_encode($data);

// 		$url="http://127.0.0.1:8000/api/emppic";
// 		$ch= curl_init();
// 		curl_setopt($ch,CURLOPT_URL,$url);
// 		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

// 		$output= curl_exec($ch);
// 		curl_close($ch);
//  		$someArray = json_decode($output, true);

// 		 // $country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
// 		 // $someArray['Country_id'] = $country;

// 		 // return view('pages.orgview',compact("someArray"));
// }  




public function empinsert()
{
			 $countryList = Country::all()->toArray();
		 	 $stateList = State::all()->toArray();
		 	  $cityList = City::all()->toArray();
		 	   $orgList = InsData::all()->toArray();

// return view('pages.empinsert',compact('stateList','countryList','cityList','orgList'));
 		return view('pages.empinsert',compact('stateList','countryList','cityList','orgList'));

	// return view('pages.empinsert');

}  


public function empinsert1(Request $request)
{


	$message = [
        'required' => 'Please fill the above field',
        'success' => 'Thank You',
];
$this->validate($request, [
         	'Org_id'=>'required',
			'Emp_name'=>'required',
			'Emp_lastname'=>'required',
			'Email'=>"required", 
			'Phone'=>'required', //|unique:employees
			'loc_add'=>'required',
			'Country_id'=>'required',
			'state_id'=>'required',
			'City_id'=>'required',
			'is_enabled'=>'required'
       ], $message);




		// $validdata=$request->validate([
		// 	'Org_id'=>'required',
		// 	'Emp_name'=>'required',
		// 	'Emp_lastname'=>'required',
		// 	'Phone'=>'required',
		// 	'loc_add'=>'required',
		// 	'Country_id'=>'required',
		// 	'state_id'=>'required',
		// 	'City_id'=>'required',
		// 	'is_enabled'=>'required'

		// ]);
		
		$Org_id=$request->input('Org_id');
		$Emp_name=$request->input('Emp_name');
		$Emp_lastname=$request->input('Emp_lastname');
		$Email=$request->input('Email');
		$Phone=$request->input('Phone');
		$loc_add=$request->input('loc_add');
		$Country_id=$request->input('Country_id');
		$state_id=$request->input('state_id');
		$City_id=$request->input('City_id');
		$is_enabled=$request->input('is_enabled');




		$Country_id=(int)$Country_id;
		$Org_id=(int)$Org_id;
		$state_id=(int)$state_id;
		$City_id=(int)$City_id;





		$data=['Org_id'=>$Org_id,'Emp_name'=>$Emp_name,
		'Emp_lastname'=>$Emp_lastname,'Email'=>$Email,'Phone'=>$Phone,
		'loc_add'=>$loc_add,'Country_id'=>$Country_id,
		'state_id'=>$state_id,'City_id'=>$City_id,'is_enabled'=>$is_enabled


	];

		$data=json_encode($data);


		$url="http://laratask.com/api/empindata";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		// curl_setopt($ch, CURLOPT_FAILONERROR, true); 
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

		  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$output= curl_exec($ch);
		
	


		curl_close($ch);
 		$someArray = json_decode($output, true);

 		if($someArray['result'] == 'fail')
           return redirect('/empinsert/'.$Emp_id)->with('message',$someArray['message']);
       else
           return redirect('/about')->with('message',$someArray['message']); 



 		// return view('pages.about');

	
}  

public function empdelete($id)
{

		$url="http://laratask.com/api/empdelete/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$output= curl_exec($ch);
		curl_close($ch);
 		$someArray = json_decode($output, true);

		 // $country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
		 // $someArray['Country_id'] = $country;
return redirect('/about');
		 	  // return view('pages.about',compact("someArray"));
		 // return view('pages.orgview',compact("someArray"));
}  


public function empview($id)
{
		$url="http://laratask.com/api/empshowid/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$output= curl_exec($ch);

// print_r($output);

// echo $output->Pic_Path;
		curl_close($ch);
 		$someArray = json_decode($output, true);

// echo gettype($someArray);
// print_r($someArray);
// exit;
// $arr=$someArray['Pic_Path'];


 		$country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
		$state = DB::table('states')->where('state_id', $someArray['state_id'])->value('State_name');

		$city = DB::table('cities')->where('City_id', $someArray['City_id'])->value('City_name');
		$org = DB::table('organizations')->where('Org_id', $someArray['Org_id'])->value('Org_name');


		$someArray['Country_id'] = $country;
		$someArray['state_id'] = $country;

		$someArray['City_id'] = $country;
		$someArray['Org_id'] = $country;








 		return view('pages.empview',compact("someArray"));
}  


public function showimg($id)
{
		$url="http://laratask.com/api/empshowid/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$output= curl_exec($ch);


		curl_close($ch);
 		$someArray = json_decode($output, true);

// echo "<pre>";print_r($someArray);

// exit;

 		$country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
		$state = DB::table('states')->where('state_id', $someArray['state_id'])->value('State_name');

		$city = DB::table('cities')->where('City_id', $someArray['City_id'])->value('City_name');
		$org = DB::table('organizations')->where('Org_id', $someArray['Org_id'])->value('Org_name');


		$someArray['Country_id'] = $country;
		$someArray['state_id'] = $country;

		$someArray['City_id'] = $country;
		$someArray['Org_id'] = $country;








 		return view('pages.showimg',compact("someArray"));
}  





  
public function empedit($id)
{

		$url="http://laratask.com/api/empshowid/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$output= curl_exec($ch);
		curl_close($ch);
 		$someArray = json_decode($output, true);

 		$country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
		$state = DB::table('states')->where('state_id', $someArray['state_id'])->value('State_name');

		$city = DB::table('cities')->where('City_id', $someArray['City_id'])->value('City_name');
		$org = DB::table('organizations')->where('Org_id', $someArray['Org_id'])->value('Org_name');


		$someArray['Country_id'] = $country;
		$someArray['state_id'] = $state;

		$someArray['City_id'] = $city;
		$someArray['Org_id'] = $org;




			$countryList = Country::all()->toArray();
		 	 $stateList = State::all()->toArray();
		 	  $cityList = City::all()->toArray();
		 	   $orgList = InsData::all()->toArray();


 		return view('pages.empedit',compact('countryList','stateList','cityList','orgList','someArray'));


 		// return view('pages.empedit',compact("someArray"));


}



public function empedit1(Request $request,$id)
{
	$message = [
        'required' => 'Please fill the above field',
        'success' => 'Thank You',
];
$this->validate($request, [
         	'Org_id'=>'required',
			'Emp_name'=>'required',
			'Emp_lastname'=>'required',
			'Email'=>'required',
			'Phone'=>'required',//|unique:employees'
			'loc_add'=>'required',
			'Country_id'=>'required',
			'state_id'=>'required',
			'City_id'=>'required',
			'is_enabled'=>'required'
       ], $message);

		// echo "string";exit;
		$Org_id=$request->input('Org_id');
		$Emp_name=$request->input('Emp_name');
		$Emp_lastname=$request->input('Emp_lastname');
			$Email=$request->input('Email');
		$Phone=$request->input('Phone');
		$loc_add=$request->input('loc_add');
		$Country_id=$request->input('Country_id');
		$state_id=$request->input('state_id');
		$City_id=$request->input('City_id');
		$is_enabled=$request->input('is_enabled');




		$Country_id=(int)$Country_id;
		$Org_id=(int)$Org_id;
		$state_id=(int)$state_id;
		$City_id=(int)$City_id;





		$data=['Org_id'=>$Org_id,'Emp_name'=>$Emp_name,
		'Emp_lastname'=>$Emp_lastname,'Email'=>$Email,'Phone'=>$Phone,
		'loc_add'=>$loc_add,'Country_id'=>$Country_id,
		'state_id'=>$state_id,'City_id'=>$City_id,'is_enabled'=>$is_enabled


	];


		$data=json_encode($data);


		$url="http://laratask.com/api/empupdate/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		// curl_setopt($ch, CURLOPT_FAILONERROR, true); 
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

		  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$output= curl_exec($ch);


		curl_close($ch);
 		$someArray = json_decode($output, true);
	


 		if($someArray['result'] == 'fail')
           return redirect('/empedit/'.$Emp_id)->with('message',$someArray['message']);
       else
           return redirect('/about')->with('message',$someArray['message']); 

	// return view('pages.orginsert');
	// return view('pages.index', compact('title'));
}  



public function orgedit($id)
{

		// return view('pages.orgedit',compact('id'));
		$url="http://laratask.com/api/showid/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$output= curl_exec($ch);
		curl_close($ch);
 		// print_r($output);

		 $someArray = json_decode($output, true);

		 // print_r($someArray);
		 $country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
		 // echo $country;
		 // echo $someArray['Country_id'];
		 $someArray['Country_id'] = $country;
		 	$organizationList = InsData::all()->toArray();

		 $countryList = Country::all()->toArray();

// print_r($someArray);exit
		

 		return view('pages.orgedit',compact("someArray",'countryList','organizationList'));



}  



public function changedp(Request $request,$id)
{
	// echo "<pre>"; print_r($request->all());
	// echo $id;
 $flag=$request->input('flag');
	// echo  $flag;

DB::table('pictures')->where('Emp_id', '=',$id)->update(['flag' => 0]);

$data=DB::table('pictures')->where('Emp_id', '=', $id)->where('Pic_Path', '=', $flag)->update(['flag' => 1]);
// echo $data;

// 

 return redirect()->back();



}



public function orgedit1(Request $request,$id)
{

  // echo "<pre>";print_r($request);
  // exit;

		$validdata=$request->validate([
			'Org_name'=>'required',
			'Country_id'=>'required'

		]);
		// echo "string";exit;
		$Org_name=$request->input('Org_name');
		$Country_id=$request->input('Country_id');



		$Country_id=(int)$Country_id;

		$data=['Org_name'=>$Org_name,'Country_id'=>$Country_id];
		$data=json_encode($data);


		$url="http://laratask.com/api/update/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
		 curl_setopt($ch, CURLOPT_POSTFIELDS, $data);


		  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$output= curl_exec($ch);


		curl_close($ch);
 		$someArray = json_decode($output, true);
	
 		// print_r($someArray);
 		// exit;


 		// return redirect('/services');


 		if($someArray['result'] == 'fail')
           return redirect('/orgedit/'.$Org_id)->with('message',$someArray['message']);
       else
           return redirect('/services')->with('message',$someArray['message']); 



	// return view('pages.orginsert');
	// return view('pages.index', compact('title'));
}  



public function orginsert()
{

				 $countryList = Country::all()->toArray();
		// return view('pages.orginsert');

 		return view('pages.orginsert',compact('countryList'));

}  

public function orginsert1(Request $request)
{

$message = [
        'required' => 'Please fill the above field',
        'success' => 'Thank You',
];
$this->validate($request, [
            'Org_name' => 'required',
            'Country_id' => 'required',
       ], $message);




		// $validdata=$request->validate([
		// 	'Org_name'=>'required',
		// 	'Country_id'=>'required'

		// ]);
		// echo "string";exit;
		$Org_name=$request->input('Org_name');
		$Country_id=$request->input('Country_id');




		$Country_id=(int)$Country_id;

		$data=['Org_name'=>$Org_name,'Country_id'=>$Country_id];
		$data=json_encode($data);
		// print_r($data);
		// exit;
           
		$url="http://laratask.com/api/insdataa";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		 curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		// curl_setopt($ch, CURLOPT_FAILONERROR, true); 
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

		  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$output= curl_exec($ch);
		


		curl_close($ch);
 		$someArray = json_decode($output, true);



 		if($someArray['result'] == 'fail')
 		{
           return redirect('/orgedit/'.$Org_id)->with('message',$someArray['message']);
 		}
  	     else{
           return redirect('/services')->with('message',$someArray['message']); 
}


	
 		  // $msg = [
     //    'message' => 'Some Message!',
     //   ];

   // return redirect('services');
	// return view('pages.orginsert');
	// return view('pages.index', compact('title'));
}  





// public function showbyid()
// {
// 	// return 'INDEX';
	
// 	return view('pages.orginsert');
// 	// return view('pages.index', compact('title'));
// }  




public function orgview($id)
{
	// return 'INDEX';
		// $orgid = DB::table('organizations')->where('Org_id', $value["Org_id"])->value('Org_id');
 			
 	// 			 $someArray[$key]["Org_id"]=$orgid;		

		//   return view('pages.orgview',compact("someArray"));
	// return view('pages.index', compact('title'));

		$url="http://laratask.com/api/showid/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$output= curl_exec($ch);
		curl_close($ch);
 		// print_r($output);

		 $someArray = json_decode($output, true);

		 // print_r($someArray);
		 $country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
		 // echo $country;
		 // echo $someArray['Country_id'];
		 $someArray['Country_id'] = $country;

		 	 // print_r($someArray);
		 // echo $someArray['Country_id'];
// 		  foreach ($someArray as $key => $value) 
// 			{
// 				var_dump($value);
// 				// $org= DB::table('organizations')->where('Org_id', $value["Org_id"])->value('Org_name');
// 		    	$country = DB::table('countries')->where('Country_id', $value["Country_id"])->value('Country_name');
//  				// $state = DB::table('states')->where('state_id', $value["state_id"])->value('State_name');
//  				// $city = DB::table('cities')->where('City_id', $value["City_id"])->value('City_name');
 
// echo $country;
// exit;

		    
// 	  		 }



		 	  return view('pages.orgview',compact("someArray"));
}  

  




// 	$message = [
//         'required' => 'Please fill the above field',
//         'success' => 'Thank You',
// ];
// $this->validate($request, [
//          	'Org_id'=>'required',
// 			'Emp_name'=>'required',
// 			'Emp_lastname'=>'required',
// 			'Email'=>"required", 
// 			'Phone'=>'required', //|unique:employees
// 			'loc_add'=>'required',
// 			'Country_id'=>'required',
// 			'state_id'=>'required',
// 			'City_id'=>'required',
// 			'is_enabled'=>'required'
//        ], $message);



public function orgdelete($id)
{

		$url="http://laratask.com/api/delete/$id";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$output= curl_exec($ch);
		curl_close($ch);
 		$someArray = json_decode($output, true);

 		// return redirect('/services');

 		// print_r($someArray);
 		// exit;

 		// if($someArray['result'] == 'pass')
   //         return redirect('/services')->with('message',$someArray['message']);



 
		 // $country = DB::table('countries')->where('Country_id', $someArray['Country_id'])->value('Country_name');
		 // $someArray['Country_id'] = $country;
  // return redirect()->route('pages.services', compact('someArray'));
		 	  // return view('pages.services',compact("someArray"));
  // return redirect('/services')->with('success','Employee (ID-'.$orgid.') saved successfully');
		 // return view('pages.services',compact("someArray"));
}  





public function about(Request $request)
{
	// return 'INDEX';

// $title="about us";	
	// return view('pages.about')->with('title',$title);
		$url="http://laratask.com/api/empshowdata";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$output= curl_exec($ch);
		curl_close($ch);

		 $someArray = json_decode($output, true);

		 $currentPage = LengthAwarePaginator::resolveCurrentPage();

       	 $collection = new Collection($someArray);
        
          $per_page = 3;
          $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();

		$someArray = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);


          $someArray->setPath($request->url());
	  return view('pages.about',compact("someArray"));

	// return view('pages.about');
}    






public function services(Request $request)
	{

		$url="http://laratask.com/api/showdata";
		$ch= curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$output= curl_exec($ch);
		curl_close($ch);
		// document.write(family[1].name);
		  $someArray = json_decode($output, true);



 		 $currentPage = LengthAwarePaginator::resolveCurrentPage();

       	 $collection = new Collection($someArray);
        
          $per_page = 3;
          $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();

		$someArray = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);


          $someArray->setPath($request->url());
       
		return view('pages.services',compact("someArray"));
		
		// return view('pages.menu')->with(['someArray' => $this->someArra]);
}    











}
