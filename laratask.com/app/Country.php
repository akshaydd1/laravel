<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

      protected $table='countries';
    // public $timestamps=true;
    protected $fillable=['Country_name','Country_id'];
    protected $primaryKey = 'Country_id';
}
