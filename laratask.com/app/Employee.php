<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table='employees';
    // public $timestamps=true;
    protected $fillable=['Org_id','Emp_name','Emp_lastname','Email','Phone','loc_add','Country_id','state_id','City_id'];
    protected $primaryKey = 'Emp_id';
}
