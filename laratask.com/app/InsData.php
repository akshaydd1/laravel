<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsData extends Model
{
    protected $table='organizations';
    // public $timestamps=true;
    protected $fillable=['Org_name','Org_id','Country_id'];
    protected $primaryKey = 'Org_id';
       public $timestamp=true;   
    
}
