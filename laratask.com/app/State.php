<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
         protected $table='states';
    // public $timestamps=true;
    protected $fillable=['Country_id','State_name'];
    protected $primaryKey = 'state_id';
}
// state_id | Country_id | State_name  