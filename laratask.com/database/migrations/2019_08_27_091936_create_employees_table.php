<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
       $table->bigIncrements('Emp_id');

 
                
 $table->biginteger('Org_id')->unsigned(); 

 $table->foreign('Org_id')->references('Org_id')->on('organizations')->onDelete('cascade')->onUpdate('cascade');

$table->string('Emp_name',50);
$table->string('Emp_lastname',50);
$table->string('Email')->unique();
$table->bigInteger('Phone');

$table->string('loc_add',50);


$table->biginteger('Country_id')->unsigned(); 

 $table->foreign('Country_id')->references('Country_id')->on('countries')->onDelete('cascade')->onUpdate('cascade');




$table->biginteger('state_id')->unsigned(); 

 $table->foreign('state_id')->references('state_id')->on('states')->onDelete('cascade')->onUpdate('cascade');


$table->biginteger('City_id')->unsigned(); 

 $table->foreign('City_id')->references('City_id')->on('cities')->onDelete('cascade')->onUpdate('cascade');


 $table->boolean('is_enabled')->default(1);
 $table->boolean('is_deleted')->default(0);


$table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
