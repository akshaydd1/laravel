<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
             $table->bigIncrements('City_id');
   
             $table->biginteger('Country_id')->unsigned(); 
      

             $table->foreign('Country_id')->references('Country_id')->on('countries')->onDelete('cascade')->onUpdate('cascade');
                  
            $table->biginteger('state_id')->unsigned(); 
      

             $table->foreign('state_id')->references('state_id')->on('states')->onDelete('cascade')->onUpdate('cascade');

                    $table->string('City_name',50);
     $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
