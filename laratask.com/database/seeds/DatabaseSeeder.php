<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {  
        $this->call(countryseeder::class);
	 $this->call(stateseeder::class);
  $this->call(cityseeder::class);
  $this->call(orgseeder::class);

  $this->call(empseeder::class);

    }	
}
