<?php

use Illuminate\Database\Seeder;

class cityseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           DB::table('cities')->insert([

        	['Country_id' => (1),'state_id' => (1),'City_name'=>('Mumbai')],
        	['Country_id' => (1),'state_id' => (2),'City_name'=>('Zashi')],
        	['Country_id' => (2),'state_id' => (3),'City_name'=>('Dresden')],
        	['Country_id' => (3),'state_id' => (4),'City_name'=>('Cali')],
        	['Country_id' => (3),'state_id' => (3),'City_name'=>('Taxes')],
        	['Country_id' => (4),'state_id' => (6),'City_name'=>('boston')],
        	
        ]);
    }
}
