<?php

use Illuminate\Database\Seeder;

class orgseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('organizations')->insert([
        	['Org_name' => ('ambab'),'Country_id'=>(1)],
        	['Org_name' => ('Google'),'Country_id'=>(3)],
        	['Org_name' => ('Barclays'),'Country_id'=>(4)],
        	['Org_name' => ('Siemens'),'Country_id'=>(3)],
        	['Org_name' => ('Microsoft'),'Country_id'=>(3)],
        ]);
    }
}
