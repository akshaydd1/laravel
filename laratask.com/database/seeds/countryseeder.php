<?php
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class countryseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
        	['Country_name' => ('India')],  	
        	['Country_name' => ('Germany')],
        	['Country_name' => ('US')],
        	['Country_name' => ('UK')]

        ]);
    }
}
