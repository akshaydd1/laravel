<?php

use Illuminate\Database\Seeder;

class empseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('employees')->insert([
        	
        	['Org_id' => (5),'Emp_name' => ('Vasu'),'Emp_lastname'=>('Sharma'),'Email'=>('asd@gmail.com'),'Phone' => (7000040000),'loc_add' => ('2/4,Bandra'),'Country_id'=>(3),'state_id' => (4),'City_id' => (4)],
        	['Org_id' => (2),'Emp_name' => ('Ashish'),'Emp_lastname'=>('Gayakwad'),'Email'=>('as@gmail.com'),'Phone' => (9000848000),'loc_add' => ('2/4,HighStreet'),'Country_id'=>(3),'state_id' => (5),'City_id' => (4)],
        ]);
    }
}
       
