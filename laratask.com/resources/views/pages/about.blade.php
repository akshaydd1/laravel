@extends('layouts.app')
@section('content')




@if(session()->has('message'))
    <div class="alert alert-success">
     
        {{ session()->get('message') }}
    </div>
@endif


<!-- Org_name
Country_name -->
    <div class="container"  style="margin-top:3rem">
          <h2>Mangage Employee</h2>
                  <a type="button" class="btn btn-dark" href="/empinsert">Add New Employee</a>
                  <!-- <a type="button" class="btn btn-dark" href="/empinsert">Add New Employee</a> -->
          <table class="table">
            <thead>
              <tr>


                <th>Emp Id</th>
                <th>Picture</th>
                
                <th>Organization</th>
                <th>Emp Name</th>
                <th>Emp Lastname</th>
                <th>Email</th>

                <th>Phone</th>
                <th>Address</th>
                <th>Enable</th>
        
          
             <!-- Emp_id | Org_id | Emp_name | Emp_lastname | Phone      | loc_add        | Country_id | state_id | City_id | is_enabled -->

              </tr>
            </thead>
            <tbody>
            


            
<?php foreach($someArray as $data) : ?>
    <!-- <tr onclick="window.location='/empshowid/{{ $data['Emp_id'] }}';">  -->
    <tr >
        <td><?php echo $data["Emp_id"]; ?> </td>
  

@if((count($data["pic_flag"]))==0)
 <td>Upload first  </td>
@endif



<?php for($i=0;$i<count($data["pic_flag"]);$i++) { ?>  

@if($data["pic_flag"][$i]==1)     
                <td>
                    <img src="{{ URL::asset('/files/'.$data['pic_col'][$i]) }}"  width="60px" height="60px"/ alt="Please Upload Your Image">
               </td>
@break

@elseif(count($data["pic_flag"])!=$i)
@continue
@else
          <td>first  </td>

@endif

<?php } ?>

          
        <td><?php echo $data["organisation"]; ?></td>
        <td><?php echo $data["Emp_name"]; ?></td>
        <td><?php echo $data["Emp_lastname"]; ?></td>
        <td><?php echo $data["Email"]; ?></td>

        <td><?php echo $data["Phone"]; ?></td>
        <td><?php echo $data["loc_add"].' / '.$data["country"].' / '.$data["state"].' / '.$data["city"] ?></td>
        <td>
            <?php 
            $enable=$data["is_enabled"];

              if($enable==1)
              {
                echo "Enable";
              }
              else{
                echo "Disable";
              }

            ?>
           
        </td>

    <td>
    <div class="btn-group">
    
    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="/empedit/{{ $data['Emp_id'] }}">Edit</a>
            <a class="dropdown-item" href="/empview/{{ $data['Emp_id'] }}">View</a>
            <a class="dropdown-item" href="/empdelete/{{ $data['Emp_id'] }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
             <a class="dropdown-item" href="/file/{{ $data['Emp_id'] }}">Add Photo</a>
        </div>
    </div>
    </td>
    </tr>
<?php endforeach; ?>


            
            </tbody>
          </table>
        </div>





{!! $someArray->links() !!}










@endsection