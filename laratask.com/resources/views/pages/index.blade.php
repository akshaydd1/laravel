@extends('layouts.app')
@section('content')
      <div class="jumbotron text-center bg-secondary">
            <h1>{{$title}}</h1>
            <p>

            	<a class="btn btn-primary btn-lg" href="/login">Login</a>
                <a class="btn btn-success btn-lg" href="/register" role="button">Register</a>
            
            </p>
             
         </div> 
@endsection