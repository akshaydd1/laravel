@extends('layouts.app')
@section('content')

<div class="container" style="margin-top:3rem">

<h3 class="well"> Edit Organisation</h3>


     <form method="post" action="/orgedit1/{{ $someArray['Org_id'] }}">
      {{method_field('put')}}
       <br><br>{{ csrf_field() }}     




          <div class="form-group col-md-4">
            <label for="exampleInputEmail1">Organization Name</label>
            <input type="text" class="form-control" id="Org_name" name="Org_name" value="<?php echo $someArray["Org_name"]; ?>" >
          </div>

        

         
          <div class="form-group col-md-4">
            <label for="exampleInputEmail1">Country Name</label>
            <select class="form-control" name="Country_id">
              @foreach($countryList as $country)
                <option value="{{$country['Country_id']}}">{{$country['Country_name']}}</option>
              @endforeach
            </select>
            
      
    <span>Previous Data: <b><?php echo $someArray["Country_id"]; ?> </b></span>
          </div>
     
         <!--  <div class="dropdown col-md-4">
              <label for="exampleInputPassword1">Country Name</label>
              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Country
              <span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a href="#">India</a></li>
                  <li><a href="#">US</a></li>
                  <li><a href="#">Uk</a></li>
                </ul>
          </div> -->
            <br><br>


          <button type="submit" class="btn btn-success">Submit</button>
    </form>





</div>






@endsection