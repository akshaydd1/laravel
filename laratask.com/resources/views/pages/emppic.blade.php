@extends('layouts.app')
@section('content')


<html lang="en">

<head>

  <title>Laravel 5.6 Multiple File Upload Example</title>

  <script src="jquery-3.4.1.min.js"></script>

  <link rel="stylesheet" href="3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>



<div class="container" style="margin-top:3rem">



@if (count($errors) > 0)

<div class="alert alert-danger">

    <strong>Sorry!</strong> There were more problems with your HTML input.<br><br>

    <ul>

      @foreach ($errors->all() as $error)

          <li>{{ $error }}</li>

      @endforeach

    </ul>

</div>

@endif



@if(session('success'))

<div class="alert alert-success">

  {{ session('success') }}

</div> 

@endif



<h3 class="well"> Image Upload</h3>

<form method="post" action="/file/{{$id}}" enctype="multipart/form-data">

  {{csrf_field()}}


 <div class="form-group col-md-6">
    <label for="exampleInputEmail1"><b>Employee id</b></label>
    <input type="text" class="form-control" id="Emp_id" name="Emp_id" value="<?php   echo $id ;?>" >

  </div>

  <div class="form-group col-md-6">
    <label for="exampleInputEmail1"><b>Flag<b></label>
    <input type="text" class="form-control" id="flag" name="flag" placeholder="Enter flag">

  </div>


    <div class="input-group hdtuto control-group lst increment" >

      <input type="file" name="Pic_Path[]" class="myfrm form-control " multiple>

      <div class="input-group-btn"> 

        <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>

      </div>

    </div>

<!--     
    <div class="clone hide">

      <div class="hdtuto control-group lst input-group" style="margin-top:10px">

        <input type="file" name="Pic_Path[]" class="myfrm form-control">

        <div class="input-group-btn"> 

          <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>

        </div>

      </div>

    </div>
 -->








<div>
  @if(count($someArray['Pic_flag'])==3)
<button type="submit" class="btn btn-success" style="margin-top:10px" disabled>Submit</button>
<br>
<span class="text text-danger">Maximum Only 3 Images Allowed </span>
  @else
    <button type="submit" class="btn btn-success" style="margin-top:10px">Submit</button>
  @endif
</div>


 
</form>        

</div>



<script type="text/javascript">

    $(document).ready(function() {

      $(".btn-success").click(function(){ 

          var lsthmtl = $(".clone").html();

          $(".increment").after(lsthmtl);

      });

      $("body").on("click",".btn-danger",function(){ 

          $(this).parents(".hdtuto control-group lst").remove();

      });

    });

</script>



</body>

</html>









@endsection