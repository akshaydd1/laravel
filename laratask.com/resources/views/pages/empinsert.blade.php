@extends('layouts.app')
@section('content')





<div class="container" style="margin-top:3rem">
  <div class="offset-md-3">
    <form method="post" action="/empinsert1" >
     <br><br>{{ csrf_field() }}   
<!--   <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Organization Name</label>
    <input type="text" class="form-control" id="org" name="Org_id" placeholder="Enter Organization">
    @error('Org_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div> -->


<h3 class="well"> Insert New Employees</h3>



   <div class="form-group col-md-4">
            <label for="exampleInputEmail1"> Organisation</label>
            <select class="form-control" name="Org_id">
              @foreach($orgList as $org)
                <option value="{{$org['Org_id']}}">{{$org['Org_name']}}</option>
              @endforeach
            </select>
          </div>




    <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Employee Name</label>
    <input type="text" class="form-control" id="name" name="Emp_name" placeholder="Enter Employee Name">
   @error('Emp_name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div>

    <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Employee Lastname</label>
    <input type="text" class="form-control" id="lname" name="Emp_lastname" placeholder="Enter Employee Lastname">
    @error('Emp_lastname')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div>

    <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" class="form-control" id="Email" name="Email" placeholder="Enter Employee Lastname">
    @error('Email')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div>




    <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Phone</label>
    <input type="text" class="form-control" id="phone" name="Phone" placeholder="Enter Phone">
   @error('Phone')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div>

    <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Local Address</label>
    <input type="text" class="form-control" id="addd" name="loc_add" placeholder="Enter Local Address">   @error('loc_add')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div>
 
<!-- 

    <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Country</label>
    <input type="text" class="form-control" id="addd" name="Country_id" placeholder="Enter Local Address">
   @error('Country_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    </div> -->



          <div class="form-group col-md-4">
            <label for="exampleInputEmail1">Country Name</label>
            <select class="form-control" name="Country_id">
              @foreach($countryList as $country)
                <option value="{{$country['Country_id']}}">{{$country['Country_name']}}</option>
              @endforeach
            </select>
          </div>


<!--     <div class="form-group col-md-6">
    <label for="exampleInputEmail1">State</label>
    <input type="text" class="form-control" id="addd" name="state_id" placeholder="Enter Local Address">
   @error('state_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    </div>
 -->


          <div class="form-group col-md-4">
            <label for="exampleInputEmail1">State Name</label>
            <select class="form-control" name="state_id">
              @foreach($stateList as $state)
                <option value="{{$state['state_id']}}">{{$state['State_name']}}</option>
              @endforeach
            </select>
          </div>


<!-- 
    <div class="form-group col-md-6">
    <label for="exampleInputEmail1">City</label>
    <input type="text" class="form-control" id="addd" name="City_id" placeholder="Enter Local Address">
   @error('City_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    </div>
 -->


       <div class="form-group col-md-4">
            <label for="exampleInputEmail1">City Name</label>
            <select class="form-control" name="City_id">
              @foreach($cityList as $city)
                <option value="{{$city['City_id']}}">{{$city['City_name']}}</option>
              @endforeach
            </select>
          </div>



      <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Enable</label>
    <input type="text" class="form-control" id="addd" name="is_enabled" placeholder="Enter Local Address">
   @error('is_enabled')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    </div>



 






    <br><br>


  <button type="submit" class="btn btn-success">Submit</button>
</form>

</div>

</div>








@endsection