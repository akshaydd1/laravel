@extends('layouts.app')
@section('content')







<div class="container" style="margin-top:3rem">


<h3 class="well"> Edit Employee</h3>



    <form method="post" action="/empedit1/{{ $someArray['Emp_id'] }}">
        {{method_field('put')}}
     <br><br>{{ csrf_field() }}



     <div class="form-group col-md-4">
            <label for="exampleInputEmail1"> Organisation</label>
            <select class="form-control" name="Org_id">
              @foreach($orgList as $org)
                <option value="{{$org['Org_id']}}">{{$org['Org_name']}}</option>
              @endforeach
            </select>

<span>Previous Data: <b><?php echo $someArray["Org_id"]; ?></b></span>
          </div>




    <div class="form-group col-md-4">
    <label for="exampleInputEmail1">Employee Name</label>
    <input type="text" class="form-control" id="name" name="Emp_name" value="<?php echo $someArray["Emp_name"]; ?>">
       @error('Emp_name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 

  </div>

  

    <div class="form-group col-md-4">
    <label for="exampleInputEmail1">Employee Lastname</label>
    <input type="text" class="form-control" id="lname" name="Emp_lastname" value="<?php echo $someArray["Emp_lastname"]; ?>">
       @error('Emp_lastname')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
 
  </div>

   <div class="form-group col-md-4">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" class="form-control" id="lname" name="Email" value="<?php echo $someArray["Email"]; ?>">
       @error('Email')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
 
  </div>


    <div class="form-group col-md-4">
    <label for="exampleInputEmail1">Phone</label>
    <input type="text" class="form-control" id="phone" name="Phone" value="<?php echo $someArray["Phone"]; ?>">
   @error('Phone')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div>

    <div class="form-group col-md-4">
    <label for="exampleInputEmail1">Local Address</label>
    <input type="text" class="form-control" id="addd" name="loc_add" value="<?php echo $someArray["loc_add"]; ?>">
       @error('loc_add')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
  </div>
 


              <div class="form-group col-md-4">
            <label for="exampleInputEmail1">Country Name</label>
            <select class="form-control" name="Country_id">
              @foreach($countryList as $country)
                <option value="{{$country['Country_id']}}">{{$country['Country_name']}}</option>
              @endforeach
            </select>

<span>Previous Data: <b><?php echo $someArray["Country_id"]; ?></b></span>

          </div>





  <div class="form-group col-md-4">
            <label for="exampleInputEmail1">State Name</label>
            <select class="form-control" name="state_id">
              @foreach($stateList as $state)
                <option value="{{$state['state_id']}}">{{$state['State_name']}}</option>
              @endforeach
            </select>

<span>Previous Data: <b><?php echo $someArray["state_id"]; ?></b></span>
          </div>

   <div class="form-group col-md-4">
            <label for="exampleInputEmail1">City Name</label>
            <select class="form-control" name="City_id">
              @foreach($cityList as $city)
                <option value="{{$city['City_id']}}">{{$city['City_name']}}</option>
              @endforeach
            </select>



<span>Previous Data: <b><?php echo $someArray["City_id"]; ?></b></span>
          </div>




      <div class="form-group col-md-4">
    <label for="exampleInputEmail1">Enable</label>
    <input type="text" class="form-control" id="addd" name="is_enabled" value="<?php echo $someArray["is_enabled"]; ?>">
       @error('is_enabled')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 

    </div>



 






    <br><br>


  <button type="submit" class="btn btn-success">Submit</button>
</form>


</div>









@endsection