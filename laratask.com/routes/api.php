	<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/insdataa','ApiController@create');

Route::get('/showdata','ApiController@show');

Route::get('/showid/{Org_id}','ApiController@showbyid');

Route::put('/update/{Org_idd}','ApiController@updatebyid');

Route::delete('/delete/{Org_idd}','ApiController@deletebyid');


Route::put('/flag/{Org_idd}','ApiController@flagid');





Route::post('/empindata','EmpApiController@create');

Route::get('/empshowdata','EmpApiController@show');

Route::get('/empshowid/{Org_id}','EmpApiController@showbyid');

Route::put('/empupdate/{Org_idd}','EmpApiController@updatebyid');

Route::delete('/empdelete/{Org_idd}','EmpApiController@deletebyid');




// Route::group(['middleware'=>'auth:api'], function () {
//     Route::apiResource('org',)
// });
