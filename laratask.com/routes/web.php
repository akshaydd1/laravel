<?php
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });




Route::any('/search',function(){
    $q = Input::get ( 'q' );


    $someArray = DB::table('organizations')->where('Org_name','LIKE','%'.$q.'%')->orWhere('Country_id','LIKE','%'.$q.'%')->get();
    if(count($someArray) > 0)
    	return $someArray;
    //     return view('pages.services')->withDetails($someArray)->withQuery ( $q );
    // else return view ('pages.services')->withMessage('No Details found. Try to search again !');
});


Route::put('changeflag/{id}','PagesController@changedp');
Route::get('/','PagesController@index');
Auth::routes();
Route::get('/about','PagesController@about');



Route::get('services','PagesController@services');





Route::get('orgview/{id}','PagesController@orgview');


Route::get('orgdelete/{id}','PagesController@orgdelete');


Route::get('orginsert','PagesController@orginsert');
Route::post('orginsert1','PagesController@orginsert1');

Route::get('orgedit/{id}','PagesController@orgedit');


Route::put('orgedit1/{id}','PagesController@orgedit1');

Route::get('showimg/{id}','PagesController@showimg');







Route::get('empdelete/{id}','PagesController@empdelete');


Route::get('empview/{id}','PagesController@empview');


Route::get('empinsert','PagesController@empinsert');
Route::post('empinsert1','PagesController@empinsert1');


Route::get('empedit/{id}','PagesController@empedit');
Route::put('empedit1/{id}','PagesController@empedit1');


// Route::get('orgdelete','PagesController@orgdelete');
Route::get('emppic/{id}','PagesController@emppic');






Route::get('changeflag/{id}','PictureController@create');



Route::get('file/{id}','PictureController@create');

Route::post('file/{id}','PictureController@store');


// Route::get('showid/{Org_id}','PagesController@showbyid');

// Route::get('/empshowid/{Org_id}','EmpApiController@showbyid');
// Route::get('/', function () {
//     return view('welcome');
// });
