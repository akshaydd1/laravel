<?php
namespace Task\Command\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Name extends Command
{
   protected function configure()
   {
       $this->setName('message:name');
       $this->setDescription('Demo command line');
        $options = [
      new InputOption('name','N',InputOption::VALUE_OPTIONAL,'name','Admin'),

      new InputOption('cat','C',InputOption::VALUE_OPTIONAL,'cat')
    ];
      $this->setDefinition($options);

   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {
        for($i=0;$i<=4;$i++)
        {
        
          $output->writeln("completely download ".$i." files from 5 files");  
          
        
        }
          $name=$input->getOption('name');
          $cat=$input->getOption('cat');
          
       $output->writeln('<info>Your downloading is complete '.$name.'! And category is :-'.$cat.'<info>');
   }
}