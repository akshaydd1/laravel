<?php
namespace Task\Command\Console;

use Symfony\Component\Console\Command\Command; 
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Sendmessage extends Command
{

    protected $_curl;



public function __construct(
\Magento\Framework\HTTP\Client\Curl $curl)
{
  $this->curl=$curl;
  parent::__construct();
}


   protected function configure()
   {
       $this->setName('message:send');
       $this->setDescription('Demo command line');

   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {

       
            $ch = curl_init("mmagento.com/rest/V1/magento-createapi/post");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
         
       $output->writeln('<info>'.print_r($result).' <info>');
   }
}