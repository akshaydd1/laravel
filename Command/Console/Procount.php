<?php
namespace Task\Command\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

use Symfony\Component\Console\Output\OutputInterface;

class Procount extends Command
{


	private $productCollectionFactory;

	public function __construct(
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,$name=null)
	{
		parent::__construct($name);
		$this->productCollectionFactory=$productCollectionFactory;
	}





   protected function configure()
   {
       $this->setName('message:prodcount');
       $this->setDescription('give product count');
        $options = [
      new InputOption('name','N',InputOption::VALUE_OPTIONAL,'name','Admin')
    ];
      $this->setDefinition($options);

   }
   
   protected function execute(InputInterface $input, OutputInterface $output)
   {

          $productcount=$this->productCollectionFactory->create()->count();
          $output->writeln(sprintf('<info>You have %s products <info>',$productcount));
   }
}