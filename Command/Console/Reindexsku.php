<?php
namespace Task\Command\Console;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Reindexsku extends Command
{
    private $productCollectionFactory;
    protected $indexerFactory;
   protected $stockRegistry;
    // protected $priceFactory;

        public function __construct(
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Indexer\IndexerRegistry $indexerFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        // \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,$name=null)
      {
        $this->stockRegistry = $stockRegistry;
        parent::__construct();
        $this->productCollectionFactory=$productCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->indexerFactory = $indexerFactory;
        // $this->priceFactory = $priceFactory;
      }



    protected function configure()
       {
           $this->setName('indexer:bysku');
           $this->setDescription('Reindex by SKU and Particular Indexer');
        $options = [
          new InputOption('Sku','S',InputOption::VALUE_OPTIONAL,'Sku'),
          new InputOption('cat','I',InputOption::VALUE_OPTIONAL,'cat'),

          // new InputOption('ind1','J',InputOption::VALUE_OPTIONAL,'ind1')
        ];//Take input SKU and INDEXER name
          $this->setDefinition($options);
    }
      protected function execute(InputInterface $input, OutputInterface $output)
   {

        $sku=$input->getOption('Sku');
        $cat=$input->getOption('cat');



       // $ind1=$input->getOption('ind1');
         $output->writeln(sprintf('<comment>Stock_Id =<comment><info>%s</info>',$cat));
        $indexerIds=explode(",",$cat);
       // $output->writeln(sprintf('<comment>Stock_Id =<comment><info>%s</info>',$ind1));
        print_r($indexerIds);



       $stockItem = $this->stockRegistry->getStockItemBySku($sku);
       $stock_id=$stockItem['stock_id'];         //Extract Stock Id 
       $output->writeln(sprintf('<comment>Stock_Id =<comment><info>%s</info>',$stock_id));
             foreach ($indexerIds as $indexerId) {
       $categoryIndexer = $this->indexerFactory->get($indexerId);//Passed Indexer Name to IndexerFactory
       $categoryIndexer->reindexRow($stock_id);//Reindex STOCK From STOCK Id

}
  


        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToFilter('sku',$sku);//filter value from SKU
        $productCollection->addAttributeToSelect(['sku']);
        $id=$productCollection->getData();//Convert All Data From Object To Array
        $pid=$id[0]['entity_id'];
        $output->writeln(sprintf('<comment>Stock_Id =<comment><info>%s</info>',$pid));


      foreach ($indexerIds as $indexerId) {

        $output->writeln(sprintf('<comment>Stock_Id =<comment><info>%s</info>',$indexerId));

        $categoryIndexer = $this->indexerFactory->get($indexerId);//Passed Indexer Name to IndexerFactory

        $categoryIndexer->reindexRow($pid);//Reindex Product From Product Id

      }



        foreach ($indexerIds as $indexerId) {
               $product = $this->_productFactory->create()->load($pid);
                $Arr_Cat = $product->getCategoryIds();
                foreach($Arr_Cat as $category) 
                {
                  $Cat_Id=$category;
                  $output->writeln(sprintf('<comment>Cat_Id =<comment><info>%s</info>',$Cat_Id));
                  $categoryIndexer = $this->indexerFactory->get($indexerId);
                  $categoryIndexer->reindexRow($Cat_Id);
                }


        }
      $output->writeln('<info>Successfully Reindex For SKU :-'.$sku.'! And category is :-'.$cat.'<info>');


       exit;





      }
}


